package unit1;

import java.util.Scanner;

public class D2Any{
    public static void main(String[] args) {

        System.out.print("Please enter a decimal number:");
        Scanner input = new Scanner(System.in);        
        int d = input.nextInt();
        System.out.print("Please enter radix:");
        int r = input.nextInt();
        input.close();
        String b = "";
        while(d > 0)
        {          
            b = d%r + b;
            d = d/r;
        }
        System.out.println(b.toString());
        
    }
}