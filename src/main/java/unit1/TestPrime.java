package unit1;

import java.util.Scanner;

public class TestPrime {
    public static void main(String[] args) throws InterruptedException {
        System.out.print("Please enter a number:");
        Scanner consoleInput = new Scanner(System.in);
        int n = consoleInput.nextInt();
        boolean isPrime = true;
        int f = 0;
        long s = System.currentTimeMillis();
        if (n % 2 == 0) {
            isPrime = false;
            f = 2;
        } else {
            for (int i = 3; (i * i) <= n; i = i + 2) {
                if (n % i == 0) {
                    isPrime = false;
                    f = i;
                    break;
                }
            }
        }
        consoleInput.close();
        if (isPrime)
            System.out.println(n + " is a prime");
        else
            System.out.println(n + " is not a prime, it can be divided by:" + f);
        long e = System.currentTimeMillis();
        System.out.println("It took me " + (e - s) + " ms to figure it out");
    }
}