package unit1;

import java.util.Scanner;

public class B2D {
    public static void main(String[] args) {
        System.out.print("Please enter a binary number:");
        Scanner input = new Scanner(System.in);
        String b =input.nextLine().trim();
        input.close();
        if(b.length() == 0)
            throw new RuntimeException("please enter a binary number!");
        long d = 0;
        int exp = b.length() -1;
        for (int i = 0; i < b.length() ; i++) {
            if (b.charAt(i) == '1')
                d += power(2,exp);
            else if (b.charAt(i) == '0') {

            } else {
                throw new RuntimeException(b + "is not a vaild binary number");
            }
            exp --;
        }
        System.out.println(b + " in decimal is " + d);
       
    }

    public static long power(int base, int exp) {
        if (base == 0)
            return 0;
        if (exp == 0)
            return 1;
        int n = base;
        long result = n;
        for (int i = 1; i < exp; i++) {
            result *= n;
        }
        return result;
    }
}