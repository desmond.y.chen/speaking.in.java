package unit1;

public class FactorFinder {
    public static void main(String[] args) {
        // long n = 1000100000010002l;
        long n = 1000100005;        
        theEasyWay(n);
        aFasterWay(n);        
    }

    private static void theEasyWay(long n) {
        long s = System.nanoTime();
        String factors = "";
        long i = 2;
        while (i <= n) {
            while (n % i == 0) {
                factors += " " + i;
                n = n / i;
            }
            i++;
        }
        long e = System.nanoTime();
        System.out.println(factors + "[E:" + (e - s) + "]");
    }

    private static void aFasterWay(long n) {
        long s = System.nanoTime();
        String factors = "";
        while (n % 2 == 0) {
            n /= 2;
            factors += " 2";
        }
        long i = 3;
        while (i <= n) {
            while (n % i == 0) {
                factors += " " + i;
                n = n / i;
            }
            if (n > 1 && i * i > n) {
                factors += " " + n;
                break;
            } else
                i += 2;
        }
        long e = System.nanoTime();
        System.out.println(factors + "[F:" + (e - s) + "]");
    }
}